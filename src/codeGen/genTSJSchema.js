const fs = require('fs');
const tsj = require('ts-json-schema-generator');

const genTSJSchema = (
  config = { path: undefined, tsconfig: undefined, type: '*' },
  createdBy = undefined,
  silent = false
) => {
  const type = config.type || '*';

  if (!fs.existsSync(config.path))
    throw Error(`File "${config.path}" not found`);

  const schema = tsj.createGenerator({ ...config, type }).createSchema(type);

  !silent && console.log(`\x1b[35m<\x1b[0m ${config.path}`);

  return {
    $createdAt: new Date(),
    ...(createdBy ? { $createdBy: `yarn ${createdBy}` } : {}),
    ...schema,
  };
};

module.exports = genTSJSchema;
