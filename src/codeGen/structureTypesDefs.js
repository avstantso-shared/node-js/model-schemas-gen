// Not needed if no generics based
const Model = {
  IID: 'Model.IID',
  IVersioned: 'Model.IVersioned',
  IDates: 'Model.IDates',
};

module.exports = function (entityName, structure) {
  const hasMaster = 'object' === structure.master?.type;

  const defs = {};

  function pType(part) {
    return structure[part] && `${entityName}['${part}']`;
  }

  function partial(type) {
    return type && `Partial<${type}>`;
  }

  function andType(...types) {
    return types.filter((t) => t).join(' & ');
  }

  if (hasMaster) defs.Master = `Omit<${pType('master')}, 'entity'>`;

  if (structure.minimal) defs.Minimal = andType(defs.Master, pType('minimal'));

  if (structure.insert) {
    defs.Insert = andType(defs.Master, pType('insert'));
    defs.Update = andType(Model.IID, Model.IVersioned, defs.Insert);
  }

  if (structure.insertSecrets) defs.InsertSecrets = pType('insertSecrets');

  if (structure.insertTree)
    defs.InsertTree = andType(defs.Insert, pType('insertTree'));

  if (hasMaster || structure.insert || structure.insertTree)
    defs.InsertDetail = andType(
      partial(defs.Master),
      pType('insert'),
      pType('insertTree')
    );

  if (structure.updateTree)
    defs.UpdateTree = andType(defs.Update, pType('updateTree'));

  if (hasMaster || structure.insert || structure.updateTree)
    defs.UpdateDetail = andType(
      Model.IID,
      Model.IVersioned,
      partial(defs.Master),
      pType('insert'),
      pType('updateTree')
    );

  defs.Select = andType(
    Model.IID,
    Model.IVersioned,
    Model.IDates,
    defs.Minimal,
    pType('insert'),
    pType('select')
  );

  defs.Condition = partial(
    andType(defs.Minimal, pType('insert'), pType('select'))
  );

  return defs;
};
