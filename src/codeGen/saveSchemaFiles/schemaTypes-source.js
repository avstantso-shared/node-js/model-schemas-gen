const { TS } = require('@avstantso/node-js--code-gen');
const { Indent, GetTypes, SubEntities } = require('./utils');

function exportNamespace(
  namespaceName,
  namespaceEntities,
  depth = 0,
  typesCode = undefined
) {
  const subEntities = SubEntities(namespaceEntities);

  if (!subEntities.length) return '';

  const indent = Indent(depth);
  const getTypes = GetTypes(namespaceEntities);

  return TS.resolve([
    indent(),
    TS.Export.Namespace(
      namespaceName,
      subEntities
        .map((entityName) => {
          function entityTypesCode() {
            return getTypes(entityName)
              .map(
                (entityType) => `${indent(2)}${entityType}: JSONSchema7;\r\n`
              )
              .join('');
          }

          const subNamespace = exportNamespace(
            entityName,
            namespaceEntities[entityName],
            depth + 1,
            entityTypesCode
          );

          return subNamespace
            ? subNamespace + '\r\n'
            : indent(1) +
                TS.Export.Interface(entityName, null, entityTypesCode());
        })
        .join('\r\n')
    ),
    '\r\n',
    indent(),
    TS.Export.Interface(
      namespaceName,
      'JSONSchema7',
      subEntities
        .map(
          (entityName) =>
            indent(1) +
            TS.Type.Field(entityName, `${namespaceName}.${entityName}`)
        )
        .join('') + (typesCode ? '\r\n' + typesCode() : '')
    ),
  ]);
}

function schemaTypesSource({ schemaTypeName, entities }) {
  return TS.resolve([
    TS.Import.Type.Members('JSONSchema7').From('json-schema'),
    '\r\n',
    exportNamespace(schemaTypeName, entities),
  ]);
}

module.exports = schemaTypesSource;
