const { TYPES } = require('../consts');

function Indent(depth = 0) {
  return (indentDepth = 0) => '  '.repeat(depth + indentDepth);
}

function GetTypes(entities) {
  return (entityName) => {
    const entity = entities[entityName];
    const { structure } = entity;

    return Object.keys(structure);
  };
}

function SubEntities(entities) {
  return !entities
    ? []
    : Object.keys(entities).filter(
        (key) => key.charAt(0) === key.charAt(0).toLocaleUpperCase()
      );
}
SubEntities.NoDetails = (entities) =>
  SubEntities(entities).filter((name) => !entities[name].master);
SubEntities.Details = (entities, masterName) =>
  SubEntities(entities).filter((name) => masterName === entities[name].master);

function hasCutters(typeName) {
  return [
    TYPES.Minimal,
    TYPES.Insert,
    TYPES.InsertTree,
    TYPES.Update,
    TYPES.UpdateTree,
  ].includes(typeName);
}

module.exports = { Indent, GetTypes, SubEntities, hasCutters };
