const { TS } = require('@avstantso/node-js--code-gen');
const { TYPES } = require('../consts');
const { Indent, GetTypes, SubEntities } = require('./utils');

const FOR_NS = 0;
const FOR_TYPE = 1;
const FOR_CONST = 2;

function tsElement(dest, cutters) {
  switch (dest) {
    case FOR_TYPE:
      return TS.Export.Interface('Cutters', '', cutters);
    case FOR_NS:
      return TS.Export.Namespace('Cutters', cutters);
    case FOR_CONST:
      return TS.Export.Const.Same('Cutters', TS.Object(cutters));
    default:
      throw Error(`Invalid dest value ${dest}`);
  }
}

function GetRH(entities) {
  const getTypes = GetTypes(entities);

  return (entityName) => {
    const types = getTypes(entityName);

    const isReadOnly = !types.find((name) => TYPES.Insert === name);

    const hasHierarchical =
      !isReadOnly && types.find((name) => TYPES.InsertTree === name);

    return { isReadOnly, hasHierarchical };
  };
}

function entityCutters(dest, entity, subPath) {
  const subEntities = SubEntities(entity);
  if (!subEntities.length) return '';

  const depth = !subPath ? 0 : subPath.split('.').length;
  const indent = Indent(depth);
  const getRH = GetRH(entity);

  return subEntities
    .map((entityName) => {
      const { isReadOnly, hasHierarchical } = getRH(entityName);

      function getTypeType(forceModel) {
        return !forceModel && SubEntities(entity[entityName]).length
          ? `Cutters.${subPath ? `${subPath}.` : ``}${entityName}`
          : `${
              isReadOnly
                ? 'Model.Cutter.Readonly'
                : hasHierarchical
                ? 'Model.Cutter.Hierarchical'
                : 'Model.Cutter.Simple'
            }<ThisModel.${subPath ? `${subPath}.` : ``}${entityName}>`;
      }

      function subCuttersNSSource() {
        const subCuttersNS = entityCutters(
          FOR_NS,
          entity[entityName],
          `${subPath ? `${subPath}.` : ``}${entityName}`
        );

        return !subCuttersNS
          ? ''
          : 0 === depth
          ? subCuttersNS
          : indent(1) + TS.Export.Namespace(entityName, subCuttersNS);
      }

      function schemaObjLine(modelType) {
        return TS.Object.Field(
          `to${modelType}`,
          TS.ArrowFunc.Short(
            '',
            'data',
            '',
            TS.Call('cutBySchema', [
              'data',
              `SCHEMA.${
                subPath ? `${subPath}.` : ``
              }${entityName}.${modelType}`,
            ])
          )
        );
      }

      function subCuttersObjsSource() {
        const subCuttersObjs = entityCutters(
          FOR_CONST,
          entity[entityName],
          `${subPath ? `${subPath}.` : ``}${entityName}`
        );
        return !subCuttersObjs ? '' : '\r\n' + subCuttersObjs;
      }

      switch (dest) {
        case FOR_TYPE:
          return TS.Type.Field([indent(1), entityName], getTypeType());
        case FOR_NS:
          return TS.resolve([
            subCuttersNSSource(),
            SubEntities(entity[entityName]).length && [
              indent(1),
              TS.Export.Type(
                entityName,
                TS.Type.Intersection(
                  getTypeType(true),
                  TS.Object(
                    entityCutters(
                      FOR_TYPE,
                      entity[entityName],
                      `${subPath ? `${subPath}.` : ``}${entityName}`
                    )
                  )
                )
              ),
            ],
          ]);
        case FOR_CONST:
          return TS.resolve([
            indent(1),
            TS.Object.Field(
              entityName,
              TS.Object([
                indent(2) + schemaObjLine(TYPES.Minimal, TYPES.Minimal),
                ...(isReadOnly
                  ? []
                  : [
                      indent(2) + schemaObjLine(TYPES.Insert),
                      indent(2) + schemaObjLine(TYPES.Update),
                      ...(!hasHierarchical
                        ? []
                        : [
                            indent(2) + schemaObjLine(TYPES.InsertTree),
                            indent(2) + schemaObjLine(TYPES.UpdateTree),
                          ]),
                    ]),
                subCuttersObjsSource(),
              ])
            ),
          ]);
        default:
          throw Error(`Invalid dest value ${dest}`);
      }
    })
    .join('');
}

function cuttersSource({ paths, entities }) {
  return TS.resolve([
    TS.Import.Members('cutBySchema, Model').From(paths.modelCoreLib),
    TS.Import.AsteriskAs('ThisModel').From(paths.modelSourceIndexFile),
    TS.Import('SCHEMA').From('./schema'),
    '\r\n',
    [FOR_NS, FOR_TYPE, FOR_CONST]
      .reduce((r, dest) => {
        const cutters = entityCutters(dest, entities);
        if (cutters) r.push(tsElement(dest, cutters));
        return r;
      }, [])
      .join('\r\n'),
  ]);
}

module.exports = cuttersSource;
