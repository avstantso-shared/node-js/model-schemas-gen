const path = require('path');
const fs = require('fs');
const Promise = require('bluebird');
const { writeSourceFile, TS } = require('@avstantso/node-js--code-gen');

const ReThrow = require('../re-throw');
const { SubEntities } = require('./utils');
const schemaTypesSource = require('./schemaTypes-source');
const schemaTSSource = require('./schemaTS-source');
// const cuttersSource = require('./cutters-source'); don`t used
const reexportEntitySource = require('./reexportEntity-source');

async function saveSchemaFiles(
  options = {
    paths: {
      modelCoreLib: undefined,
      modelSourceIndexFile: undefined,
      fieldsInfosFile: undefined,
    },
    outputDir: undefined,
    schemaName: undefined,
    schema: undefined,
    entities: undefined,
    parentSchemas: undefined,
    fieldsInfos: undefined,
    sysData: undefined,
    reExportHooks: undefined,
  },
  scriptName = undefined,
  silent = false
) {
  const {
    paths,
    outputDir,
    schemaName,
    schema,
    entities,
    parentSchemas,
    fieldsInfos,
    sysData,
    reExportHooks,
  } = options;

  const outputPath = path.resolve(outputDir, schemaName);
  if (!fs.existsSync(outputPath)) fs.mkdirSync(outputPath);

  const schemaTypeName = `${schemaName
    .charAt(0)
    .toUpperCase()}${schemaName.slice(1)}Schema`;

  const writeFile = async (fileNameRel, content, ...subpaths) =>
    writeSourceFile(
      path.resolve(outputPath, ...subpaths, fileNameRel),
      content,
      '.ts' === path.extname(fileNameRel) ? scriptName : null,
      { silent }
    );

  async function saveSchemaJson() {
    return writeFile('schema.json', JSON.stringify(schema));
  }

  async function saveSchemaTypes() {
    return writeFile(
      'schema-types.ts',
      schemaTypesSource({ schemaTypeName, entities })
    );
  }

  async function saveSchemaTs() {
    return writeFile(
      'schema.ts',
      schemaTSSource({ paths, parentSchemas, schemaTypeName, entities })
    );
  }

  // don`t used
  // async function saveCutters() {
  //   return writeFile('cutters.ts', cuttersSource({ paths, entities }));
  // }

  async function saveReexport() {
    const reexportOutputPath = path.resolve(outputPath, 're-export');
    if (!fs.existsSync(reexportOutputPath)) fs.mkdirSync(reexportOutputPath);

    const noDetails = SubEntities.NoDetails(entities);

    await Promise.mapSeries(noDetails, (entityName) =>
      writeFile(
        `${entityName.charAt(0).toLowerCase() + entityName.slice(1)}.ts`,
        ReThrow({ entityName }, () =>
          reexportEntitySource({
            paths,
            entities,
            entityName,
            fieldsInfos,
            sysData,
            hooks:
              'function' === typeof reExportHooks
                ? reExportHooks()
                : reExportHooks,
          })
        ),
        're-export'
      )
    );

    {
      const source = TS.resolve([
        noDetails.map((entityName) =>
          TS.Import.Members(entityName).From(
            `./${entityName.charAt(0).toLowerCase() + entityName.slice(1)}`
          )
        ),
        '\r\n',
        TS.Export.Object(noDetails) + '\r\n',
        '\r\n',
        TS.Export.Const(
          'Fields',
          TS.Object(
            noDetails.map((entityName) =>
              TS.Object.Destr(`${entityName}.Fields`)
            )
          )
        ),
      ]);

      return writeFile(`index.ts`, source, 're-export');
    }
  }

  async function saveIndex() {
    const source =
      TS.Export.Asterisk.From('./schema-types') +
      TS.Export.Asterisk.From('./schema') +
      TS.Export.Asterisk.From('./re-export');

    return writeFile('index.ts', source);
  }

  try {
    await saveSchemaJson();
    await saveSchemaTypes();
    await saveSchemaTs();
    // await saveCutters(); don`t used
    await saveReexport();
    await saveIndex();
  } catch (e) {
    // e.context = { ...(e.context || {}), entities };
    throw e;
  }
}

module.exports = saveSchemaFiles;
