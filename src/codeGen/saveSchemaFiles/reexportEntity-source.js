const { TS } = require('@avstantso/node-js--code-gen');
const ReThrow = require('../re-throw');
const { GetTypes, SubEntities, hasCutters } = require('./utils');

function hooksMerge(hooks, entityName, hookName, ...hookFields) {
  if (!hooks || !hookFields.length) return;

  const top = hooks[hookName];
  const entity = hooks[entityName] && hooks[entityName][hookName];

  const r = {};
  hookFields.forEach((field) => {
    const h = TS.resolve([top && top[field], entity && entity[field]]);
    if (h) r[field] = h;
  });

  return Object.keys(r).length ? r : undefined;
}
hooksMerge.Import = (hooks, entityName) =>
  hooksMerge(hooks, entityName, 'import', 'before', 'utils', 'after');

function Hook(hooks, ...hookHierarchyName) {
  if (!hooks) return;

  if ('object' !== typeof hooks) throw Error('Hook "hooks" must be object');

  let hook = hooks;
  function recursion(index) {
    if (index >= hookHierarchyName.length) return;

    const name = hookHierarchyName[index];
    hook = hook[name];
    if (!hook) return;

    const next = recursion(index + 1);
    return { name: next || hook };
  }
  const context = { hooks: recursion(0) };

  return !hook ? undefined : TS.resolve(ReThrow(context, () => hook));
}
Hook.Wrap = (hooks, wrap, name, ...hookHierarchyName) => {
  const hook = Hook(hooks, name, ...hookHierarchyName);
  return !hook ? undefined : wrap(name, hook);
};

function WrapTS(entityName, isTypes) {
  return (name, entities, iteration) => {
    const content = TS.resolve(entities.map(iteration), '\r\n');
    return isTypes
      ? TS.Region(`${entityName} ${name}`, content)
      : TS.Const(
          name,
          TS.ArrowFunc.Inline([
            content,
            '\r\n',
            TS.Return(TS.Object(entities.join(','))),
          ])
        );
  };
}

function entityReexport(staticContext, dynamicContext) {
  const { fieldsInfos, sysData, isTypes } = staticContext;
  const { entities, entityName, subPath } = dynamicContext;

  const fieldInfo = fieldsInfos && fieldsInfos[entityName];
  const hooks = dynamicContext.hooks && dynamicContext.hooks[entityName];

  const depth = !subPath ? 0 : subPath.split('.').length;
  const getTypes = GetTypes(entities);
  const entityTypes = getTypes(entityName);
  const entityPath = `${subPath ? `${subPath}.` : ``}${entityName}`;
  const entity = entities[entityName];
  const reexportPath = `${
    entity.master && !entityPath.startsWith(`${entity.master}.`)
      ? `${entity.master}.`
      : ''
  }${entityPath}`;

  const wrapTS = WrapTS(entityName, isTypes);

  const subs = {
    Details: {
      entities: SubEntities.Details(entities, entityName),
      recursion: (detailName) =>
        ReThrow({ detailName }, () =>
          entityReexport(staticContext, {
            ...dynamicContext,
            entityName: detailName,
          })
        ),
    },
    SubEntities: {
      entities: SubEntities(entity),
      recursion: (subEntityName) =>
        ReThrow({ subEntityName }, () => {
          const content = entityReexport(staticContext, {
            entities: entity,
            entityName: subEntityName,
            subPath: entityPath,
            hooks,
          });

          return isTypes
            ? TS.Region(`${entityPath}.${subEntityName}`, content)
            : content;
        }),
    },
  };

  function processSubs(isShort) {
    return Object.keys(subs)
      .filter((subsKey) => subs[subsKey].entities.length)
      .map((subsKey) => {
        if (isShort) return TS.Object.Destr(subsKey);

        const { entities, recursion } = subs[subsKey];

        return TS.resolve(['\r\n', wrapTS(subsKey, entities, recursion)]);
      });
  }

  function typesSource() {
    function entitySubCustomTypesSources() {
      if (!entity.meta.subCustomTypes) return [];

      // TODO: add namespaced subCustomTypes support

      return Object.keys(entity.meta.subCustomTypes).map((subCustomType) =>
        ReThrow({ subCustomType }, () => {
          const hook =
            hooks?.subCustomTypes && hooks.subCustomTypes[subCustomType];

          return TS.Export.Type(
            subCustomType,
            hook
              ? 'function' === typeof hook
                ? hook(subCustomType, entityPath)
                : hook
              : `InitialModel.${entityPath}.${subCustomType}`
          );
        })
      );
    }

    return [
      // Show commented entities
      // TS.Comment.Block(JSON.stringify(entities, null, 2)),
      // '\r\n',

      TS.Export.Type(
        entityName,
        Hook(hooks?.type, 'replace') || `InitialModel.${entityPath}`
      ),
      '\r\n',

      TS.Export.Namespace(entityName, [
        TS.Region(
          `${entityName} types`,
          [
            ...entitySubCustomTypesSources(),
            ...entityTypes.map((entityType) =>
              ReThrow({ entityType }, () =>
                TS.resolve([
                  TS.Export.Type(entityType, `${entityPath}['${entityType}']`),
                  Hook.Wrap(
                    hooks?.types,
                    TS.Export.Namespace,
                    entityType,
                    'namespace'
                  ),
                ])
              )
            ),
          ].join('\r\n')
        ),
        TS.concat('\r\n', Hook(hooks, 'namespace'), '\r\n'),
        processSubs(),
      ]),
      ,
    ];
  }

  function dataSource() {
    function entitySysDataSource() {
      const entitySysData = sysData && sysData[entityName];

      if (!entitySysData) return '';

      const sysDataSource = `extractGeneratedData(_SYS_DATA${
        entitySysData.resolve ? `.${entitySysData.resolve}` : ''
      })`;

      return TS.resolve([
        TS.Const(
          'SysData',
          entitySysData.value ||
            (entitySysData.generator &&
              `(${entitySysData.generator})(${sysDataSource})`) ||
            TS.Object(TS.Object.Destr(sysDataSource), entitySysData.type)
        ),
        '\r\n',
      ]);
    }

    return (depth > 0 || entity.master ? TS.Const : TS.Export.Const)(
      entityName,
      TS.ArrowFunc.Inline([
        TS.Const('_Model', reexportPath, 'null'),
        TS.Const('name', `'${reexportPath}'`),
        '\r\n',
        wrapTS('Types', entityTypes, (entityType) =>
          ReThrow({ entityType }, () =>
            TS.Const(
              entityType,
              TS.ArrowFunc.Inline([
                TS.Const('Schema', `_SCHEMA.${entityPath}.${entityType}`),
                TS.Const(
                  'Fields',
                  TS.Object(
                    TS.Object.Destr(
                      TS.Call(
                        'fieldsBySchema',
                        `_SCHEMA_RAW.definitions${
                          depth ? `['${entityPath}']` : `.${entityPath}`
                        }.properties.${entityType}`
                      )
                    )
                  )
                ),
                hasCutters(entityType) &&
                  TS.Const(
                    'Cutter',
                    `Model.Cutter.To${entityType}<${reexportPath}>`,
                    TS.ArrowFunc.Short(
                      '',
                      'data',
                      '',
                      `cutBySchema(data, _SCHEMA.${entityPath}.${entityType})`
                    )
                  ),
                TS.concat('\r\n', Hook(hooks?.types, entityType, 'const')),
                '\r\n',
                TS.Const(
                  'type',
                  TS.Object(
                    TS.resolve(
                      ['Schema', 'Fields', hasCutters(entityType) && 'Cutter'],
                      ','
                    )
                  )
                ),
                '\r\n',
                TS.Call.Gen(
                  'Model.Declaration.Type.Check',
                  [
                    reexportPath,
                    `Model.Declaration.Types.${entityType}<${reexportPath}>`,
                  ],
                  'type'
                ) + '\r\n',
                '\r\n',
                TS.Return('type'),
              ])
            )
          )
        ),
        processSubs(),
        '\r\n',
        TS.Const('Schema', `_SCHEMA.${entityPath}`),
        fieldInfo && TS.Const('FieldsInfo', `_FieldsInfos.${entityPath}`),
        '\r\n',
        TS.Const('Fields', () => {
          const personalFields = TS.Object(
            entityTypes
              .map((entityType) => `...Types.${entityType}.Fields`)
              .join(',')
          );

          const [detailsFields, subEntitiesFields] = Object.keys(subs)
            // .filter((subsKey) => subs[subsKey].entities.length)
            .map((subsKey) =>
              subs[subsKey].entities
                .map((name) => `...${subsKey}.${name}.Fields`)
                .join(',')
            );

          if (!(detailsFields.length + subEntitiesFields.length))
            return personalFields;

          return TS.ArrowFunc.Inline(
            TS.resolve(
              [
                TS.Const('PersonalFields', personalFields),
                detailsFields.length &&
                  TS.Const('DetailsFields', TS.Object(detailsFields)),
                subEntitiesFields.length &&
                  TS.Const('SubEntitiesFields', TS.Object(subEntitiesFields)),
                TS.Return(
                  TS.Object([
                    TS.Object.Destr('PersonalFields'),
                    detailsFields.length && TS.Object.Destr('DetailsFields'),
                    subEntitiesFields.length &&
                      TS.Object.Destr('SubEntitiesFields'),
                  ])
                ),
              ],
              '\r\n\r\n'
            )
          );
        }),
        '\r\n',
        entitySysDataSource(),

        TS.concat('\r\n', Hook(hooks, 'beforeDeclaration'), '\r\n'),

        TS.Const(
          'declaration',
          TS.Object([
            TS.Object.Field('Model', '_Model'),
            TS.resolve(
              [
                'name',
                'Schema',
                'Fields',
                fieldInfo && 'FieldsInfo',
                sysData && sysData[entityName] && 'SysData',
                '...Types',
              ],
              ','
            ),
            ',',
            processSubs(true),
            TS.concat('\r\n', Hook(hooks, 'declaration'), '\r\n'),
          ])
        ),
        '\r\n',
        TS.Call.Gen(
          'Model.Declaration.Check',
          [reexportPath, `Model.Declaration<${reexportPath}>`],
          'declaration'
        ) + '\r\n',
        '\r\n',
        TS.Return('declaration') + ';',
      ])
    );
  }

  return TS.resolve(isTypes ? typesSource : dataSource);
}

function reexportEntitySource({
  paths,
  entities,
  entityName,
  fieldsInfos,
  sysData,
  hooks,
}) {
  const fieldInfo = fieldsInfos && fieldsInfos[entityName];

  const entitySysData = sysData && sysData[entityName];

  const hooksImport = hooksMerge.Import(hooks, entityName);

  return TS.resolve([
    TS.concat(Hook(hooksImport, 'before'), '\r\n'),
    hooksImport?.utils &&
      TS.Import.Members([Hook(hooksImport, 'utils')]).From(
        '@avstantso/node-or-browser-js--utils'
      ),
    TS.Import.Members('Model, fieldsBySchema, cutBySchema').From(
      paths.modelCoreLib.startsWith('..')
        ? `../${paths.modelCoreLib}`
        : paths.modelCoreLib
    ),
    '\r\n',
    TS.Import.AsteriskAs('InitialModel').From(
      `../${paths.modelSourceIndexFile}`
    ),
    fieldInfo &&
      TS.Import('_FieldsInfos').From(
        `../${
          paths.fieldsInfosFile ||
          `${paths.modelSourceIndexFile}/fieldsInfos.json`
        }`
      ),
    '\r\n',
    entitySysData &&
      (entitySysData.import ||
        TS.Import('_SYS_DATA').From(`../${entitySysData.path}`)),
    TS.Import.Members('SCHEMA as _SCHEMA').From('../schema'),
    TS.Import('_SCHEMA_RAW').From('../schema.json'),
    '\r\n',
    TS.concat(Hook(hooksImport, 'after'), '\r\n'),
    TS.resolve(
      [true, false].map((isTypes) =>
        ReThrow({ isTypes }, () =>
          entityReexport(
            {
              fieldsInfos,
              sysData,
              isTypes,
            },
            {
              entities,
              entityName,
              hooks,
            }
          )
        )
      ),
      '\r\n'
    ),
  ]);
}

module.exports = reexportEntitySource;
