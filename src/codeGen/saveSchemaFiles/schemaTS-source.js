const { TS } = require('@avstantso/node-js--code-gen');

const { Indent, GetTypes, SubEntities } = require('./utils');

function entitySchema(entity, subPath) {
  const subEntities = SubEntities(entity);
  if (!subEntities.length) return '';

  const depth = !subPath ? 0 : subPath.split('.').length;
  const indent = Indent(depth);
  const getTypes = GetTypes(entity);

  return subEntities
    .map((entityName) => {
      const subEntitySchema = entitySchema(
        entity[entityName],
        `${subPath ? `${subPath}.` : ``}${entityName}`
      );

      return TS.Object.Field(
        [indent(1), entityName],
        TS.Object([
          subEntitySchema + '\r\n',
          ...getTypes(entityName).map((entityType) =>
            TS.Object.Field(
              [indent(2), entityType],
              TS.Call(
                'ss',
                `'${subPath ? `${subPath}.` : ''}${entityName}.${entityType}'`
              )
            )
          ),
        ])
      );
    })
    .join('\r\n');
}

function schemaTSSource({ paths, parentSchemas, schemaTypeName, entities }) {
  return TS.resolve([
    TS.Import.Type.Members('JSONSchema7').From('json-schema'),
    '\r\n',
    TS.Import.Members('extractGeneratedData').From(
      '@avstantso/node-or-browser-js--utils'
    ),
    TS.Import.Members('subSchema').From(paths.modelCoreLib),
    ...(parentSchemas || []).map(({ path, name, alias }) =>
      TS.Import.Members(`${name}${alias ? ` as ${alias}` : ''}`).From(
        path || paths.modelCoreLib
      )
    ),
    '\r\n',
    TS.Import.Type.Members(schemaTypeName).From('./schema-types'),
    TS.Import('GeneratedSchema').From('./schema.json'),
    '\r\n',
    TS.Const('RAW_SCHEMA', 'extractGeneratedData(GeneratedSchema)'),
    '\r\n',
    TS.Const(
      'clearSchema',
      TS.Object(
        [
          TS.Object.Destr('RAW_SCHEMA'),
          TS.Object.Field(
            'definitions',
            TS.Object(
              [
                ...(parentSchemas || []).map(
                  ({ name, alias, resolve }) =>
                    `...${alias || name}${
                      resolve ? `.${resolve}` : ''
                    }.definitions`
                ),
                TS.Object.Destr('RAW_SCHEMA.definitions'),
              ].join(',\r\n')
            )
          ),
        ],
        'JSONSchema7'
      )
    ),
    TS.Function(
      'ss',
      '',
      'key: string',
      'JSONSchema7',
      TS.Return(
        TS.Call('subSchema', [
          'RAW_SCHEMA.definitions[key as keyof object]',
          'clearSchema',
        ])
      ) + ';'
    ),
    '\r\n',
    TS.Export.Const(
      'SCHEMA',
      TS.Object(
        [TS.Object.Destr('clearSchema'), entitySchema(entities)],
        schemaTypeName
      )
    ),
  ]);
}

module.exports = schemaTSSource;
