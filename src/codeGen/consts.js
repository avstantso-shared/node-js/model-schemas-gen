const tsFileRegEx = /.ts$/;
const entityInternalNameRegEx = /Internal/i;
const ignoreEintitiesFilesNamesMatchs = [/\.ts/];
const ignoreEintitiesFilesNamesNotMatchs = [/index\.ts/];
const structureRegEx = /^Structure/;
const typeMetaRegEx = /\.Meta/;

const TYPES = {
  Meta: 'Meta',
  Master: 'Master',
  Minimal: 'Minimal',
  Insert: 'Insert',
  InsertSecrets: 'InsertSecrets',
  InsertTree: 'InsertTree',
  InsertDetail: 'InsertDetail',
  Update: 'Update',
  UpdateTree: 'UpdateTree',
  UpdateDetail: 'UpdateDetail',
  Select: 'Select',
  Condition: 'Condition',
};

const defaultDefs = [TYPES.Insert, TYPES.Update, TYPES.Select].map(
  (s) => `#/definitions/${s}`
);

module.exports = {
  tsFileRegEx,
  entityInternalNameRegEx,
  ignoreEintitiesFilesNamesMatchs,
  ignoreEintitiesFilesNamesNotMatchs,
  structureRegEx,
  typeMetaRegEx,
  TYPES,
  defaultDefs,
};
