function ReThrow(context, next) {
  try {
    return 'function' === typeof next ? next(context) : next;
  } catch (e) {
    e.context = {
      ...(e.context || {}),
      ...context,
    };
    throw e;
  }
}

module.exports = ReThrow;
