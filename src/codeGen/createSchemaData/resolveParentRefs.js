// Not needed if no generics based
module.exports = function (definitions) {
  Object.keys(definitions).forEach((typeName) => {
    const typeDef = definitions[typeName];
    const ref = typeDef['$ref'];
    if (!ref) return;

    const refTypeName = ref
      .replace('#/definitions/', '')
      .replace('%3C', '<')
      .replace('%3E', '>');
    if (definitions[refTypeName]) return;

    const parentType = parentSchemas.find(
      ({ schema: parentSchema }) => parentSchema.definitions[refTypeName]
    )?.schema?.definitions[refTypeName];
    if (!parentType) throw Error(`Parent type "${refTypeName}" not found`);

    definitions[typeName] = { ...parentType };
  });
};
