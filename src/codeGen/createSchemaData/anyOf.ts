// Not needed if no generics based
module.exports = function (definitions) {
  Object.keys(definitions).forEach((typeName) => {
    const typeDef = definitions[typeName];
    const anyOf = typeDef['anyOf'];
    if (!anyOf) return;

    definitions[typeName] = { ...anyOf[0], additionalProperties: false };
  });
};
