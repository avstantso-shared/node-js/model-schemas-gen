const ReThrow = require('../re-throw');

module.exports = function (entities, fieldsInfos) {
  Object.keys(entities).forEach((entityName) =>
    ReThrow({ entityName }, () => {
      const entityFieldsInfo = fieldsInfos[entityName];
      if (!entityFieldsInfo) return;

      const { structure } = entities[entityName];
      Object.keys(structure).forEach((entityType) =>
        ReThrow({ entityType }, () => {
          const { properties } = structure[entityType];
          if (!properties) return;

          Object.keys(entityFieldsInfo).forEach((fieldName) =>
            ReThrow({ fieldName }, () => {
              const fieldSchema = properties[fieldName];
              if (!fieldSchema) return;

              const fieldInfo = entityFieldsInfo[fieldName];

              properties[fieldName] = {
                ...fieldSchema,
                ...(fieldInfo.length?.min
                  ? { minLength: fieldInfo.length.min }
                  : {}),
                ...(fieldInfo.length?.max
                  ? { maxLength: fieldInfo.length.max }
                  : {}),
                ...(fieldInfo.pattern ? { pattern: fieldInfo.pattern } : {}),
              };
            })
          );
        })
      );
    })
  );
};
