const { typeMetaRegEx, TYPES, defaultDefs } = require('../consts');
const ReThrow = require('../re-throw');

function separateTypesNames(definitions) {
  const metaKeys = Object.keys(definitions).filter((name) =>
    typeMetaRegEx.test(name)
  );

  const entityNames = metaKeys.map((metaName) =>
    metaName.replace(typeMetaRegEx, '')
  );

  const noEntityTypes = Object.keys(definitions).filter(
    (name) =>
      !(
        entityNames.includes(name) ||
        Object.keys(TYPES).find((type) => name.endsWith(`.${type}`))
      )
  );

  return { metaKeys, entityNames, noEntityTypes };
}

function extractMeta(definitions, entityName) {
  const entityMetaName = `${entityName}.${TYPES.Meta}`;
  const { properties: meta } = definitions[entityMetaName];
  delete definitions[entityMetaName];

  return meta;
}

function inject2MetaNoEntityTypes(
  definitions,
  noEntityTypes,
  entityName,
  meta
) {
  function tail(name) {
    return name.substring(entityName.length + 1);
  }

  const subCustomTypesName = noEntityTypes.filter(
    (name) =>
      name.includes('.') &&
      name.startsWith(entityName) &&
      !tail(name).includes('.')
  );
  if (!subCustomTypesName.length) return;

  const subCustomTypes = {};

  subCustomTypesName.forEach(
    (name) => (subCustomTypes[tail(name)] = definitions[name])
  );

  meta.subCustomTypes = subCustomTypes;
}

function extractEntities(definitions) {
  const { entityNames, noEntityTypes } = separateTypesNames(definitions);

  const entities = {};

  entityNames.forEach((entityName) =>
    ReThrow({ entityName }, () => {
      const meta = extractMeta(definitions, entityName);
      inject2MetaNoEntityTypes(definitions, noEntityTypes, entityName, meta);

      const master = meta.master?.properties?.entity?.const;

      const structure = definitions[entityName].properties;
      Object.keys(structure).forEach((key) => {
        const part = structure[key];

        if (
          key.includes(' ') ||
          !Object.values(part).filter((v) => v && !defaultDefs.includes(v))
            .length
        ) {
          delete structure[key];
          return;
        }

        // A.V.Stantso forced set "additionalProperties"
        part.additionalProperties = false;

        definitions[`${entityName}.${key}`] = {
          $ref: `#/definitions/${entityName}/properties/${key}`,
        };
      });

      const entityNameParts = entityName.split('.');
      let subEntities = entities;
      for (let i = 0; i < entityNameParts.length - 1; i++)
        subEntities = subEntities[entityNameParts[i]];

      subEntities[entityNameParts[entityNameParts.length - 1]] = {
        meta: meta,
        ...(master ? { master } : {}),
        structure,
        ...(subEntities[entityNameParts[entityNameParts.length - 1]] || {}),
      };
    })
  );

  return entities;
}

module.exports = extractEntities;
