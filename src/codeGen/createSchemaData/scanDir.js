const path = require('path');
const fs = require('fs');
const {
  ignoreEintitiesFilesNamesMatchs,
  ignoreEintitiesFilesNamesNotMatchs,
} = require('../consts');
const genTSJSchema = require('../genTSJSchema');

function readdirRecursion(inputDir, subPath) {
  return fs.readdirSync(inputDir).reduce((r, item) => {
    const itemPath = path.resolve(inputDir, item);

    function itemSubPath() {
      return subPath ? `${subPath}/${item}` : item;
    }

    if (fs.lstatSync(itemPath).isDirectory())
      r.push(...readdirRecursion(itemPath, itemSubPath()));
    else if (
      ignoreEintitiesFilesNamesMatchs.every((r) => r.test(item)) &&
      !ignoreEintitiesFilesNamesNotMatchs.some((r) => r.test(item))
    )
      r.push(itemSubPath());

    return r;
  }, []);
}

module.exports = function (inputDir, tsconfig, silent) {
  const entitiesForScan = readdirRecursion(inputDir);

  if (!entitiesForScan.length)
    console.log(
      `\x1b[33mWarning!\x1b[0m Entities list is empty for "${inputDir}"`
    );
  else
    !silent &&
      console.log(
        `\x1b[35m<\x1b[0m For scan in ${inputDir}:\r\n%O`,
        entitiesForScan
      );

  let r = {};

  entitiesForScan.map((entityFileName) => {
    const entitySchema = genTSJSchema({
      path: path.resolve(inputDir, entityFileName),
      tsconfig,
    });

    r = { ...r, ...entitySchema.definitions };
  });

  return r;
};
