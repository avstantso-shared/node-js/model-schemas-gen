const path = require('path');
const fs = require('fs');

const clear = require('../clear');

const scanDir = require('./scanDir');
const extractEntities = require('./extractEntities');
const injectFieldsInfos = require('./injectFieldsInfos');

function createScemaData(
  options = {
    inputDir: undefined,
    tsconfig: undefined,
    schemaName: undefined,
    parentSchemas: [],
    generics: true,
    saveAnyOf: false,
    fieldsInfos: undefined,
  },
  scriptName = undefined,
  silent = false
) {
  const {
    inputDir,
    tsconfig,
    parentSchemas,
    schemaName,
    generics,
    saveAnyOf,
    fieldsInfos,
  } = options || {
    parentSchemas: [],
    generics: true,
  };

  const definitions = scanDir(inputDir, tsconfig, silent);

  parentSchemas.forEach(({ schema: parentSchema }) =>
    clear.duplicates(definitions, parentSchema)
  );

  clear.structure(definitions);
  clear.internal(definitions);

  const entities = extractEntities(definitions);

  if (fieldsInfos) injectFieldsInfos(entities, fieldsInfos);

  const curSchema = {
    $createdAt: new Date(),
    $createdBy: `yarn ${scriptName}`,
    $schema: 'http://json-schema.org/draft-07/schema#',
    $id: schemaName,
    definitions,
  };

  !generics && clear.generics(curSchema);

  // #region fixSchema
  (() => {
    const fixSchemaFile = path.resolve(inputDir, 'fixSchema.js');
    if (fs.existsSync(fixSchemaFile)) {
      const fix = require(fixSchemaFile);
      fix(curSchema, entities);
    }
  })();
  // #endregion

  return {
    schema: curSchema,
    entities,
    ...(parentSchemas?.length ? { parentSchemas } : {}),
    ...(fieldsInfos ? { fieldsInfos } : {}),
  };
}

module.exports = createScemaData;
