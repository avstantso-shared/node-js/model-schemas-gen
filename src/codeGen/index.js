const genTSJSchema = require('./genTSJSchema');
const clear = require('./clear');
const internalSaveSchemaFiles = require('./saveSchemaFiles');
const internalCreateScemaData = require('./createSchemaData');

const CodeGen = (
  generatorOptions = {
    tsconfig: undefined,
    scriptName: undefined,
    silent: false,
  }
) => {
  const { tsconfig, scriptName, silent } = generatorOptions;

  function genSchema(options = { path: undefined, type: '*' }) {
    return genTSJSchema(
      { tsconfig, type: '*', ...options },
      scriptName,
      silent
    );
  }

  function createScemaData(
    options = {
      inputDir: undefined,
      tsconfig: undefined,
      schemaName: undefined,
      parentSchemas: [],
      generics: true,
      saveAnyOf: false,
      fieldsInfos: undefined,
    }
  ) {
    return internalCreateScemaData(
      { tsconfig, ...options },
      scriptName,
      silent
    );
  }

  async function saveSchemaFiles(
    options = {
      paths: {
        modelCoreLib: undefined,
        modelSourceIndexFile: undefined,
        fieldsInfosFile: undefined,
      },
      outputDir: undefined,
      schemaName: undefined,
      schema: undefined,
      entities: undefined,
      fieldsInfos: undefined,
      sysData: undefined,
      reExportHooks: undefined,
    }
  ) {
    const { paths, ...rest } = options;
    return internalSaveSchemaFiles(
      {
        paths: {
          modelCoreLib: paths.modelCoreLib
            ? paths.modelCoreLib
            : '@avstantso/node-or-browser-js--model-core',
          modelSourceIndexFile: paths.modelSourceIndexFile,
          fieldsInfosFile: paths.fieldsInfosFile,
        },
        ...rest,
      },
      scriptName,
      silent
    );
  }

  return { genSchema, saveSchemaFiles, createScemaData, clear };
};

module.exports = CodeGen;
