const { entityInternalNameRegEx, structureRegEx } = require('./consts');

const clearDuplicates = (dest, source) => {
  const dd = dest.definitions || dest;
  const sd = source.definitions || source;

  const sdKeys = Object.keys(sd);
  const ddKeys = Object.keys(dd);

  const toDel = ddKeys.filter((key) => sdKeys.includes(key));

  toDel.forEach((key) => delete dd[key]);
};

const clearStatic = (dest, filter) => {
  const dd = dest.definitions || dest;

  const ddKeys = Object.keys(dd);

  const toDel = ddKeys.filter(filter);

  toDel.forEach((key) => delete dd[key]);
};

const clearInternal = (dest) =>
  clearStatic(dest, (key) => entityInternalNameRegEx.test(key));

const clearStructure = (dest) =>
  clearStatic(dest, (key) => structureRegEx.test(key));

const clearGenerics = (dest) => clearStatic(dest, (key) => key.includes('<'));

module.exports = {
  duplicates: clearDuplicates,
  static: clearStatic,
  internal: clearInternal,
  structure: clearStructure,
  generics: clearGenerics,
};
